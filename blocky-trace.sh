#!/bin/bash
argv="$0 $@" # $# == ${#argv[@]} + 1
last=${argv[$#]}

# echo ${argv} > /home/marko/Ephemeral/junk/bla.txt
# echo fawefwae >> /home/marko/Ephemeral/junk/bla.txt
# echo $@ >> /home/marko/Ephemeral/junk/bla.txt

newwidth=$(( $(identify -format "%w" -- "$last") * 10))
newheight=$(( $(identify -format "%h" -- "$last") * 10))

# select every arg but last and argv[0]
mkbitmap -n -s 1 -o - -- "$last" |				# -n turns off filtering
	convert -scale "${newwidth}x${newheight}" -- - - |	# convert does a blocky scale
	/usr/bin/potrace "${argv[@]:1:$#-1}" -r 10 -a 0 -- -
